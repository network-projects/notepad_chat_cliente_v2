from tkinter import *
from ClienteSocket import *

class Menu:

    def __init__(self):

        self.Menu = Tk()
        self.Menu.title( "Notepad Chat" )
        self.Menu.geometry( '400x200' )
        self.Menu.resizable( False, False )
        self.Menu["bg"] = "GRAY"

        self.Canvas = Canvas( self.Menu )
        self.Canvas["bg"] = "GRAY"
        self.Canvas["highlightbackground"] = "BLACK"
        self.Canvas["width"] = 800
        self.Canvas.pack()

        self.labelIp = Label( self.Canvas )
        self.labelIp["text"] = "IP"
        self.labelIp["bg"] = "GRAY"
        self.labelIp.place( x = 30, y = 30 )

        self.ip = Entry( self.Canvas )
        self.ip["bg"] = "GRAY"
        self.ip["fg"] = "BLACK"
        self.ip["highlightbackground"] = "BLACK"
        self.ip["bd"] = 0
        self.ip["width"] = 20
        self.ip.place( x = 80, y = 30 )

        self.labelPorta = Label( self.Canvas )
        self.labelPorta["text"] = "Porta"
        self.labelPorta["bg"] = "GRAY"
        self.labelPorta.place( x = 30, y = 60 )

        self.porta = Entry( self.Canvas )
        self.porta["bg"] = "GRAY"
        self.porta["fg"] = "BLACK"
        self.porta["highlightbackground"] = "BLACK"
        self.porta["bd"] = 0
        self.porta["width"] = 20
        self.porta.place( x = 80, y = 60 )

        self.labelNome = Label( self.Canvas )
        self.labelNome["text"] = "Nome"
        self.labelNome["bg"] = "GRAY"
        self.labelNome.place( x = 30, y = 90 )

        self.nome = Entry( self.Canvas )
        self.nome["bg"] = "GRAY"
        self.nome["fg"] = "BLACK"
        self.nome["highlightbackground"] = "BLACK"
        self.nome["bd"] = 0
        self.nome["width"] = 20
        self.nome.place( x = 80, y = 90 )

        self.conectar = Button( self.Canvas, command = self.conectar )
        self.conectar["highlightbackground"] = "WHITE"
        self.conectar["bd"] = 0
        self.conectar["width"] = 17
        self.conectar["text"] = "Conectar"
        self.conectar.place( x = 80, y = 130 )

        self.Menu.mainloop()

    def conectar( self ):

        host = self.ip.get()
        porta = int( self.porta.get() )
        nome = self.nome.get()

        self.Canvas.pack_forget()

        conectar = ClienteSocket( host, porta, nome, self.Menu, self.Canvas )
