from tkinter import *
from socket import *
from Cliente import *
import threading
import sys
import time

class ClienteSocket:

    def __init__( self, Ip, Porta, Nome, Mapa, CanvasAnterior ):

        self.ip = Ip
        self.porta = Porta
        self.nome = Nome
        self.mapa = Mapa
        self.Canvas = CanvasAnterior
        #self.ObjetoSocket = 1

        print( self.ip, self.porta, self.nome )

        self.ObjetoSocket = socket( AF_INET, SOCK_STREAM )
        self.ObjetoSocket.connect((self.ip, self.porta))

        self.ObjetoSocket.sendall( self.nome.encode() )

        cliente = Cliente(self.ip, self.porta, self.nome, self.mapa, self.Canvas, self.ObjetoSocket)
        cliente.start()
