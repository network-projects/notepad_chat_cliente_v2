from tkinter import *
import threading

class Cliente(threading.Thread):

    def __init__( self, Ip, Porta, Nome, Mapa, CanvasAnterior, ObjetoSocket):

        threading.Thread.__init__(self)

        Mapa.geometry('1200x700')

        self.ip = Ip
        self.porta = Porta
        self.nome = Nome
        self.notepad_chat = Mapa;
        self.Canvas = CanvasAnterior
        self.ObjetoSocket = ObjetoSocket

        self.labelLeitura = Label( self.notepad_chat )
        self.labelLeitura["bg"] = "GRAY"
        self.labelLeitura["fg"] = "WHITE"
        self.labelLeitura["text"] = "MENSAGENS >> "
        self.labelLeitura.place( x = 0, y = 0 )

        self.leitura = Text( self.notepad_chat )
        self.leitura["bg"] = "WHITE"
        self.leitura["fg"] = "BLACK"
        self.leitura["width"] = 70
        self.leitura["heigh"] = 48
        self.leitura["bd"] = 2
        self.leitura["highlightbackground"] = "BLACK"
        self.leitura.place( x = 0, y = 20 )
        self.leitura.config( state = DISABLED )

        self.labelEscrita = Label( self.notepad_chat )
        self.labelEscrita["bg"] = "GRAY"
        self.labelEscrita["fg"] = "WHITE"
        self.labelEscrita["text"] = "ESCREVA >> "
        self.labelEscrita.place( x = 500, y = 0 )

        self.escrita = Text( self.notepad_chat )
        self.escrita["bg"] = "WHITE"
        self.escrita["fg"] = "BLACK"
        self.escrita["width"] = 70
        self.escrita["heigh"] = 48
        self.escrita["bd"] = 2
        self.escrita["highlightbackground"] = "BLACK"
        self.escrita.place( x = 500, y = 20 )

        self.enviar = Button( self.notepad_chat, command = self.enviarMensagem )
        self.enviar["highlightbackground"] = "WHITE"
        self.enviar["bd"] = 0
        self.enviar["width"] = 17
        self.enviar["text"] = "ENVIAR"
        self.enviar.place( x = 1020, y = 650 )

        self.labelNOME = Label( self.notepad_chat )
        self.labelNOME["bg"] = "GRAY"
        self.labelNOME["fg"] = "WHITE"
        self.labelNOME["text"] = "Nome >> " + self.nome
        self.labelNOME.place( x = 1040, y = 20 )

    def run( self ):

        while True:
            self.receberMensagem()

    def enviarMensagem( self ):

        mensagem = self.escrita.get(0.0, END)

        self.ObjetoSocket.sendall( mensagem.encode() )

    def receberMensagem( self ):

        self.leitura.delete(0.0, END)

        mensagem = self.ObjetoSocket.recv( 4096 )

        self.leitura.config( state = NORMAL )
        self.leitura.insert(INSERT, mensagem.decode('utf-8'))
        self.leitura.config( state = DISABLED )
